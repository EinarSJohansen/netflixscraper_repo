from scripts import Utility
from scripts.Scraper import HtmlScraper
from scripts.PageDownloader import PageDownloader

'''Hent data fra to kilder:
https://www.netflix.com/viewingactivity
- Denne har to faner

https://www.netflix.com/AccountAccess
'''


def RunScraper(filename = None):
	filnameBase = "Data/"

	if filename is None:
		filename2 = filnameBase + "raw.html"
	else:
		filename2 = filnameBase + filename

	scraper = HtmlScraper.HtmlNETFLIXSCRAPER(filename2)
	success = scraper.ParseFile()
	if success:
		print()
		print("Scraping was successful")

def RunPageDownloader(runDownAndScrape = False):

	address = "https://www.netflix.com/viewingactivity"

	print("write username")
	username = input()
	print("write password")
	password = input()


	filename = PageDownloader.DownloadViewingActivityPage(url=address, username=username, password=password)
	if runDownAndScrape is True:
		RunScraper(filename)

def RunAnalyser():
	print("NOT YET IMPLEMENTED")




print("Scrape [s] or Analyse [a]?")
userInput = input("").lower()

if userInput == "s" or userInput == "scrape":

	print("Update data from web? [Y] or [N]")
	inputDownload = input("").lower()

	print("> Scraping")
	if inputDownload == "y":
		RunPageDownloader(True)
	else:
		print("do you already have downloaded data? [Y] or [N]")
		hasData = input().lower()
		if hasData == "y":
			print("write username:")
			username = input()
			filename = Utility.GetFilenameFromUsername(username)
			RunScraper(filename)

		else:
			RunScraper()

elif userInput == "a" or userInput == "analyse":
	print("> Analyser")
	RunAnalyser()
else:
	print("* Error * ")

