try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup



def GetSpecificElement(html, type, attrName, attrValue):
    elem = html.find(type, attrs={attrName, attrValue})
    return elem


def GetSpecificChildren(html, type, attrName, attrValue):
    children = html.findChildren(type, attrs={attrName, attrValue})
    return children


def GetTextFromSpecificElement(html, type, attrName, attrValue):
    elem = GetSpecificElement(html=html, type=type, attrName=attrName, attrValue=attrValue)
    return elem.getText()





def GetTitleIdAndLinkFromCol(titleCol):

    titleA = titleCol.find("a")
    title = titleA.getText()

    titleLink = titleA["href"]
    titleId = titleLink.rsplit("/", 1)[1]

    return title, titleId, titleLink



def GetSeriesNameFromTitle(title):
    colonCount = title.count(":")

    seriesName = title
    seasonNum = "not found"
    episodeName = "not found"

    if colonCount >= 2:
        titleSplit = title.split(":", 2)

        seriesName = titleSplit[0].strip()
        seasonNum = titleSplit[1].strip()
        episodeName = titleSplit[2].strip()

    return seriesName, seasonNum, episodeName


