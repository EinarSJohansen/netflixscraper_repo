import json
from scripts.Scraper import ScrapingUtility as ScrUtil, htmlItemScraper, imdbRetriever

try:
	from BeautifulSoup import BeautifulSoup
except ImportError:
	from bs4 import BeautifulSoup


# vurder dette
# https://medium.com/@Mpierrax/web-scraping-with-python-b56a0e907302
# eller
# https://stackoverflow.com/questions/44239822/urllib-request-urlopenurl-with-authentication

class NetflixItem():
	def __init__(self):
		self.title = ""
		self.date = ""
		self.titleId = ""
		self.titleLink = ""
		self.year = ""
		self.genreList = []
		self.maturityRating = ""
		self.duration = ""


class HtmlNETFLIXSCRAPER:

	def __init__(self, filename):
		self.filename = filename
		self.childrenDict = {}
		self.itemScraper = htmlItemScraper.ItemScraper()
		self.imdbRet = imdbRetriever.IMDBRetriver()
		return

	def ParseFile(self):
		print("Starting scraping process")

		# parsed_html = BeautifulSoup(open("../Data/raw.html"), "lxml")
		parsed_html = BeautifulSoup(open(self.filename), "lxml")

		# find root
		rootUl = ScrUtil.GetSpecificElement(html=parsed_html,
											type="ul",
											attrName="class",
											attrValue="structural retable stdHeight")

		# find all children in root
		children = ScrUtil.GetSpecificChildren(html=rootUl,
											   type="li",
											   attrName="class",
											   attrValue='retableRow')

		# store data into dictionary
		self._LoopItems(children=children)

		return True



	def _LoopItems(self, children):

		'''
		todo:
		 > hent duration for episoder i en sesong
		'''

		for child in children:
			item = NetflixItem()

			# dato
			item.date = ScrUtil.GetTextFromSpecificElement(child, "div", "class", "col date nowrap")

			# title & ID
			titleCol = ScrUtil.GetSpecificElement(child, "div", "class", "col title")

			item.title, item.titleId, item.titleLink = ScrUtil.GetTitleIdAndLinkFromCol(titleCol)

			hasData = False
			isSeries = False
			seriesName = item.title
			seasonNum = "not found"
			episodeName = "not found"

			if ":" in item.title:
				seriesName, seasonNum, episodeName = ScrUtil.GetSeriesNameFromTitle(item.title)

				if "Sesong" in seasonNum:
					isSeries = True

				if seriesName in self.childrenDict:
					tSeries = self.childrenDict[seriesName]

					if seasonNum in tSeries["Seasons"]:
						hasData = True
						episodeObj = {
							"ItemTitle": item.title,
							"DateWatched": item.date,
							"ItemTitleLink": item.titleLink,
							"ItemTitleId": item.titleId,
						}
						seasons = self.childrenDict[seriesName]["Seasons"]
						seasons[seasonNum]["Episodes"].append(episodeObj)

			print(item.title)

			if hasData is False:

				item.year = ""
				item.genreList = []
				item.maturityRating = ""
				item.duration = ""

				itemPageObject = self.itemScraper.parseItemPage(item.titleLink)

				if itemPageObject is not None:
					item.year = itemPageObject["year"].strip()
					item.genreList = itemPageObject["genre"]
					item.maturityRating = itemPageObject["maturity"]
					item.duration = itemPageObject["duration"]

				if "TV Programmes" in item.genreList or "Serier" in item.genreList:
					isSeries = True

				if isSeries:
					self.SaveSeriesToDict(item=item, seriesName=seriesName, seasonNum=seasonNum)
				else:
					self.SaveMovieToDict(item)

			self._SaveToJSON()


	def _SaveToJSON(self):
		# save as json
		with open("Data/data.json", "w") as outfile:
			json.dump(self.childrenDict, outfile)


	def SaveSeriesToDict(self, item, seriesName, seasonNum):
		episodeObj = {
			"ItemTitle": item.title,
			"DateWatched": item.date,
			"ItemTitleLink": item.titleLink,
			"ItemTitleId": item.titleId,
		}

		tempSeason = {
			"ItemYearCreated": item.year,
			"ItemGenre": item.genreList,
			"ItemMaturityRating": item.maturityRating,
			"Episodes": []
		}

		tempSeason["Episodes"].append(episodeObj)

		if seriesName in self.childrenDict:
			seasons = self.childrenDict[seriesName]["Seasons"]
			seasons[seasonNum] = tempSeason
		else:
			tempSeries = {
				"ItemDuration": item.duration,
				"Seasons": {
					seasonNum: tempSeason
				}
			}

			self.childrenDict[seriesName] = tempSeries

	def SaveMovieToDict(self, item):

		tempElement = {
			"ItemTitle": item.title,
			"DateWatched": item.date,
			"ItemTitleLink": item.titleLink,
			"ItemTitleId": item.titleId,
			"ItemYearCreated": item.year,
			"ItemGenre": item.genreList,
			"ItemMaturityRating": item.maturityRating,
			"ItemDuration": item.duration
		}

		key = item.title

		if item.year is not None or item.year is not "":
			key += " (" + item.year + ")"

		self.childrenDict[key] = tempElement
