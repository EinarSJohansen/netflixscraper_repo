import urllib3
import certifi

from scripts.Scraper import ScrapingUtility as ScrUtil

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


class ItemScraper:

    def __init__(self):
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
        self.baseUrl = 'https://www.netflix.com/no/'
        return


    def parseItemPage(self, url):
        response = self.http.request("GET", self.baseUrl + url)

        parsed_html = BeautifulSoup(response.data, "lxml")

        selectedRoot = ScrUtil.GetSpecificElement(html=parsed_html,
                                                  type="div",
                                                  attrName="class",
                                                  attrValue="meta-content")
        if selectedRoot is None:
            return None

        # year and stuff ''' , duration'''
        year, maturityRating, duration = self._GetYearMaturityAndSeasons(selectedRoot)

        # genre
        genreList = self._GetGenre(selectedRoot)

        obj = {
            "year": year,
            "maturity": maturityRating,
            "duration" : duration,
            "genre": genreList
        }

        return obj

    def _GetYearMaturityAndSeasons(self, selectedRoot):
        # year and duration
        yearAndDurationP = ScrUtil.GetSpecificElement(html=selectedRoot,
                                                      type="p",
                                                      attrName="class",
                                                      attrValue="year-and-duration")

        year = ScrUtil.GetTextFromSpecificElement(yearAndDurationP, "span", "class", "year")
        maturityRating = ScrUtil.GetTextFromSpecificElement(yearAndDurationP, "span", "class", "maturity-number")

        durationSpan = ScrUtil.GetSpecificElement(yearAndDurationP, "span", "class", "duration")
        duration = ""


        if durationSpan.getText() is None:
            duration = ScrUtil.GetTextFromSpecificElement(yearAndDurationP, "span", "class", "test_dur_str")
        else:
            duration = durationSpan.getText()

        return year, maturityRating, duration


    def _GetGenre(self, selectedRoot):
        genre = ScrUtil.GetTextFromSpecificElement(selectedRoot,
                                                   "span",
                                                   "class",
                                                   "genre-list")

        genreList = genre.replace('\xa0', "").split(",")

        return genreList
