import imdb

class IMDBRetriver():
	def __init__(self):
		self.ia = imdb.IMDb()
		return


	def _GetIMDBData(self, title):
		s_result = self.ia.search_movie(title)

		if s_result is None or len(s_result) < 1:
			return None

		print(s_result)
		firstResult = s_result[0]

		self.ia.update(firstResult)

		plot = firstResult['plot'][0]
		plot = plot.split("::")[0]

		rating = firstResult['rating']

		#runtime = firstResult['runtime']


		imdbObject = {
			"plot" : plot,
			"rating" : rating,
		#	"runtime" : runtime
		}


		return imdbObject