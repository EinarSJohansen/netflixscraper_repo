from scripts import Utility
from selenium import webdriver
import time


def DownloadViewingActivityPage(url, username, password):
	driver = webdriver.Chrome(executable_path=r'driver/chromedriver.exe',)
	driver.get(url=url)

	# log inn to account
	print("Log inn")
	time.sleep(0.5)

	email_field = driver.find_elements_by_xpath('//*[@id="email"]')[0]
	email_field.send_keys(username)
	psw_field = driver.find_elements_by_xpath('//*[@id="password"]')[0]
	psw_field.send_keys(password)

	time.sleep(0.5)
	login_button = driver.find_elements_by_xpath('//*[@id="appMountPoint"]/div/div[3]/div/div/form[1]/button')[0]
	login_button.click()

	#click on the load more button until no more can be loaded
	time.sleep(3)
	load_more_button = driver.find_elements_by_xpath('//*[@id="appMountPoint"]/div/div/div[2]/div/div/div[2]/div/button')[0]
	while load_more_button.is_enabled():
		load_more_button.click()

	# get the item list
	list_of_items = driver.find_elements_by_xpath('//*[@id="appMountPoint"]/div/div/div[2]/div/div/ul')[0]
	html = list_of_items.get_attribute('outerHTML')

	#write to file
	filename = Utility.GetFilenameFromUsername(username)

	try:
		with open("Data/" + filename, "w+", encoding='utf-8') as f:
			f.write(html)

	except Exception as e:
		print("FAILED")
		print(e)
		print("-----------")

		driver.close()
		return None


	driver.close()
	return filename